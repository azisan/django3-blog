# This is simple blog built with django 3 learn from packtpub


Using postgresql to do search vector, search rank, etc

Installation : 

  - Clone this repo in directory
  - Create virtualenv with this command => `virtualenv envblog`
  - Activate envblog => `source envblog/bin/activate`
  - Change dir => `cd django3-blog/mysite`
  - Install dependencies => `pip install -r requirements.txt`
  - Create postgres database with name `blog_post`
  - Setup database in `mysite/settings.py` named with `DATABASES` in line 92, make sure NAME, USER, PASSWORD, HOST is valid
  - Migrate and load data `python manage.py migrate && python manage.py loaddata db.json`
  - Run server => `python manage.py runserver`

Super user :

  - Username : admin
  - Password : admin